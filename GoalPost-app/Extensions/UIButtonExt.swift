//
//  UIButtonExt.swift
//  GoalPost-app
//
//  Created by Jenia on 7/1/18.
//  Copyright © 2018 Yevheniia Krasotina. All rights reserved.
//

import UIKit

extension UIButton {
    func setSelectedColor() {
     self.backgroundColor = #colorLiteral(red: 0.2531843064, green: 0.7139427069, blue: 1, alpha: 1)
        
    }
    func setDeselectedColor() {
      self.backgroundColor = #colorLiteral(red: 0.7413883307, green: 0.8578017548, blue: 1, alpha: 1)
    }
}
