//
//  GoalType.swift
//  GoalPost-app
//
//  Created by Jenia on 7/1/18.
//  Copyright © 2018 Yevheniia Krasotina. All rights reserved.
//

import Foundation

enum GoalType: String {
    case longTerm = "Long Term"
    case shortTerm = "Short Term"
}
